#!/bin/bash

echo "the second positional argument is $2"

if [[ $1 = "me" ]]
then
  echo "I am awesome like hell"
elif [[ $1 = "them" ]]
then
  echo "They are awesome like ....."
else
  echo "Wrong argument given. [usag]: $0 [argument (Enter => me or them)]"
fi
